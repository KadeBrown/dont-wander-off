﻿using UnityEngine;
using System.Collections.Generic;

public class WallCheck : MonoBehaviour {

    public List<GameObject> walls = null;
    private float counter = 0;
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
		Debug.DrawLine(this.transform.parent.position, this.transform.position);
        if (walls.Count != 0)
        {
            counter += Time.deltaTime;
            if(counter > 0.5f)
            {
                foreach(GameObject obj in walls)
                {
                    obj.GetComponent<MeshRenderer>().material.color = new Color(obj.gameObject.GetComponent<MeshRenderer>().material.color.r, obj.gameObject.GetComponent<MeshRenderer>().material.color.g, obj.gameObject.GetComponent<MeshRenderer>().material.color.b, 1); 
                }
                counter = 0;
                walls.Clear();
            }

        }

		RaycastHit[] hits = Physics.RaycastAll(transform.position, ((this.transform.parent.position + new Vector3(0,2,0)) - this.transform.position).normalized, 
			(this.transform.parent.position - this.transform.position).magnitude);

		foreach (RaycastHit hit in hits)
		{
			if (hit.collider.gameObject.tag == "Floor" || hit.collider.gameObject.tag == "Wall")
			{
				//walls.Clear();
				walls.Add(hit.collider.gameObject);
				hit.collider.gameObject.GetComponent<MeshRenderer>().material.color = new Color(hit.collider.gameObject.GetComponent<MeshRenderer>().material.color.r, hit.collider.gameObject.GetComponent<MeshRenderer>().material.color.g, hit.collider.gameObject.GetComponent<MeshRenderer>().material.color.b, 0.6f);
			}
		}	
	}
}
