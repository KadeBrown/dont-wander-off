﻿using UnityEngine;
using System.Collections;

public class Player_Movement : MonoBehaviour
{
	public float speed = 20;
	public float health = 100;
	public float colourCoolDown;
	public GameObject deathCanvas = null;
	public GameObject pauseCanvas = null;
	public GameObject materialHolder;

	private float maxHealth;
	private float faceSpeed = 0;
	private float totalSpeed;
	private Rigidbody rb;
	private Animator anim;
	
	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody>();
		Physics.gravity = new Vector3(0, -30, 0);
		anim = GetComponentInChildren<Animator>();
		totalSpeed = speed;
		maxHealth = health;
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		//This is where she dies!
		if (this.transform.GetComponentInChildren<Player_Shield>().shieldActive == false && this.GetComponent<Player_Movement>().health < 1)
		{
			GameObject[] temp_deathCleanUp = GameObject.FindGameObjectsWithTag("Enemy");
			GameObject[] temp_deathCleanUp2 = GameObject.FindGameObjectsWithTag("EnemySpawner");
			GameObject[] temp_deathCleanUp3 = GameObject.FindGameObjectsWithTag("Wall");
			//delete each enemy that has been killed
			foreach (GameObject enemy in temp_deathCleanUp)
			{
				Destroy(enemy);
			}
			//delete each spawner that need to be deleted
			foreach (GameObject spawner in temp_deathCleanUp2)
			{
				Destroy(spawner);
			}
			//delete each wall that needs to be deleted
			foreach (GameObject wall in temp_deathCleanUp3)
			{
				Destroy(wall);
			}

			//camera zooms in on death
			GameObject mainCam = GameObject.FindGameObjectWithTag("MainCamera");
			mainCam.transform.localPosition = new Vector3(0, 4.11f, -4.55f);
			mainCam.GetComponent<Camera>().orthographic = false;
			mainCam.transform.LookAt(this.transform.position + new Vector3(0, 2, 0));

			//set position to the object you wish to spawn at and reset all spells
			GetComponent<Player_Respawn>().isDead = true;

			this.GetComponentInChildren<SpellController>().currentSpell = SpellController.Mode.None;
			this.GetComponentInChildren<SpellController>().RemoveParticles();
			this.GetComponentInChildren<SpellController>().enabled = false;
			this.GetComponentInChildren<Look_Factor>().enabled = false;
			this.GetComponent<Player_Shield>().enabled = false;
			this.GetComponent<Rigidbody>().useGravity = false;
			materialHolder.GetComponent<SkinnedMeshRenderer>().material.color = Color.white;

			//removing Heads Up Display
			GameObject mainUi = GameObject.FindGameObjectWithTag("UIMain");
			Destroy(mainUi);
			deathCanvas.SetActive(true);

			//disable player movement
			this.GetComponent<Player_Movement>().enabled = false;
			anim.SetBool("isDead", true);
		}

		//move up when 'w' is pressed
		if (Input.GetKey(KeyCode.W))
		{
			faceSpeed = Vector3.Dot(transform.forward, transform.GetChild(1).forward);
			faceSpeed = (faceSpeed < 0.5f ? 1.0f : faceSpeed * 2.0f);
			rb.MovePosition(rb.position + (transform.forward * totalSpeed * Time.deltaTime * faceSpeed));
			anim.SetBool("isWalking", true);
		}
		//move down when 's' is pressed
		if (Input.GetKey(KeyCode.S))
		{
			faceSpeed = Vector3.Dot(-transform.forward, transform.GetChild(1).forward);
			faceSpeed = (faceSpeed < 0.5f ? 1.0f : Mathf.Abs(faceSpeed * 2.0f));
			rb.MovePosition(rb.position + (-transform.forward * totalSpeed * Time.deltaTime * faceSpeed));
			anim.SetBool("isWalking", true);
		}
		//move left when 'a' is pressed
		if (Input.GetKey(KeyCode.A))
		{
			faceSpeed = Vector3.Dot(-transform.right, transform.GetChild(1).forward);
			faceSpeed = (faceSpeed < 0.5f ? 1.0f : faceSpeed * 2.0f);
			rb.MovePosition(rb.position + (-transform.right * totalSpeed * Time.deltaTime * faceSpeed));
			anim.SetBool("isWalking", true);
		}
		//move right when 'd' is pressed
		if (Input.GetKey(KeyCode.D))
		{
			faceSpeed = Vector3.Dot(transform.right, transform.GetChild(1).forward);
			faceSpeed = (faceSpeed < 0.5f ? 1.0f : faceSpeed * 2.0f);
			rb.MovePosition(rb.position + (transform.right * totalSpeed * Time.deltaTime * faceSpeed));
			anim.SetBool("isWalking", true);
		}

		//pause the game when 'c' is pressed
		if (Input.GetKeyDown(KeyCode.C))
		{
			pauseCanvas.SetActive(true);
			Time.timeScale = 0;
		}

		if (faceSpeed > 1.5f)
		{
			anim.SetBool("isBackwards", false);
			anim.SetBool("isSideways", false);
		}
		else if (faceSpeed < 0.5f)
		{
			anim.SetBool("isBackwards", true);
			anim.SetBool("isSideways", false);
		}
		else
		{
			anim.SetBool("isSideways", true);
			anim.SetBool("isBackwards", false);
		}
	}

	void Update()
	{
		//start walking animation when w, a, s or d is pressed
		if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.A))
		{
			anim.SetBool("isWalking", true);
		}

		if (!Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.W))
		{
			anim.SetBool("isWalking", false);
		}

		//manages how long you are red for upon taking damage
		if (colourCoolDown > 0)
		{
			colourCoolDown -= Time.deltaTime;
		}
		else if (colourCoolDown < 0)
		{
			colourCoolDown = 0;
			if (materialHolder.GetComponent<MeshRenderer>().Equals(null))
			{
				materialHolder.GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
			}
			else
			{
				materialHolder.GetComponent<MeshRenderer>().material.color = Color.white;
			}
		}

		//limits max health
		if (health > maxHealth)
		{
			health = maxHealth;
		}
	}

	/// <summary>
	/// deals a selected amount of damage to the player
	/// </summary>
	/// <param name="dmg"> the damage to be dealt to the player</param>
	void DealDamage(float dmg)
	{
		this.health -= dmg;

		colourCoolDown = 0.1f;
		materialHolder.GetComponent<SkinnedMeshRenderer>().material.color = Color.red;
	}
}
