﻿using UnityEngine;
using System.Collections;

public class Player_Respawn : MonoBehaviour {

    public GameObject spawnLocation;

    public bool isDead
    {
        //if the bool is set/changed
        set
        {
            //get all collider within distance of 5
            Collider[] hits = Physics.OverlapSphere(transform.position, 5);

            //each enemy within the radius of 5
            foreach (Collider c in hits)
            {
                if (c.gameObject.tag == "Enemy")
                {
                    //push enemies away
                    Rigidbody body = c.gameObject.GetComponent<Rigidbody>();
                    Vector3 dir = (c.transform.position - transform.position).normalized;
                    body.AddForce(dir * 100, ForceMode.Impulse);
                }
            }
        }
        get
        {
            return false;
        }
    }
	
}
