﻿using UnityEngine;
using System.Collections;

public class Spell_FireBall : MonoBehaviour
{

    public GameObject fireball;
    public float coolDown = 0;
    public float coolDownLess = 0;
    public float maxRange = 10;
    public GameObject spawnPos;

    private float coolDownHolder = 0;



    // Use this for initialization
    void Start()
    {
        coolDownHolder = coolDown;
        coolDownLess = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (fireball == null)
        {
            return;
        }

        if (coolDownHolder <= coolDown - coolDownLess)
        {
            coolDownHolder += Time.deltaTime;
        }

    }

	/// <summary>
	/// returns the cooldown holders value
	/// </summary>
	/// <returns></returns>
    public float GetCooldown()
    {
        return coolDownHolder;
    }

	/// <summary>
	/// Shoots a fireball to the mouse cursor on the plane
	/// </summary>
    public void UseFireBall()
    {
        this.GetComponent<Animator>().SetBool("isAttacking", true);

        coolDownHolder = 0;
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            //Debug.DrawLine(this.transform.position, targetPoint,Color.black);

            if ((targetPoint - this.transform.position).magnitude > maxRange)
            {
                targetPoint = this.transform.position + (this.transform.forward * maxRange);
            }
			//creates the fireball at the given location
            GameObject clone = Instantiate(fireball, spawnPos.transform.position + this.transform.forward, this.transform.rotation) as GameObject;
            clone.GetComponent<FireBall_Movement>().target = targetPoint;
        }

    }
}
