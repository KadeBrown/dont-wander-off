﻿using UnityEngine;
using System.Collections;

public class FireBallController : MonoBehaviour {

	public float life = 0.3f;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		life -= Time.deltaTime;
		this.transform.localScale *= 1.08f;

		if (life < 0)
		{
			Destroy(gameObject);
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			other.gameObject.SendMessage("DealDamage", 100);
		}


	}
}
