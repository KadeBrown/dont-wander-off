﻿using UnityEngine;
using System.Collections;

public class IceSpell_Movement : MonoBehaviour {

	public float bulletSpeed = 100;
	public float bulletLife = 1;
	public GameObject coneObject = null;

	private Rigidbody rb;
	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void FixedUpdate()
	{
		//this.transform.Rotate()
		rb.MovePosition(this.transform.position + this.transform.forward * bulletSpeed * Time.deltaTime);

		bulletLife -= Time.deltaTime;

		if (bulletLife < 0)
		{
			Destroy(gameObject);
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy")
		{
			//other.gameObject.SendMessage("DealDamage", 50);
			Instantiate(coneObject, this.transform.position, this.transform.rotation);
		}
		Destroy(gameObject); // this allows the bullet to be destroyed on contact with terrain or anything with a collider
	}
}
