﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextScreen : MonoBehaviour
{
	public TextAsset textfile;
	public GameObject textHolder;
	public bool isWall = false;
	public string[] texts;
	public int[] lineCount;
    public GameObject iconHolder = null;
    public GameObject background = null;

	public Sprite[] sprites;

	private string temp;
    private bool isCounting = false;
    private float counter = 5;
	private int countLines = 0;
	// Use this for initialization
	void Start()
	{
		texts = textfile.ToString().Split("\n"[0]);
	}

    void Update()
    {
        if(isCounting == true)
        {
            counter -= Time.deltaTime;
			if (counter < 0 && countLines + 1 < lineCount.Length)
			{
				Debug.Log(countLines);
				countLines++;
				Debug.Log(countLines);
				counter = 5;
				temp = texts[lineCount[countLines]];
				int temp_num = int.Parse(temp[0].ToString());
				iconHolder.GetComponent<Image>().sprite = sprites[temp_num];

				temp = temp.Trim(temp[0]);
				textHolder.GetComponent<Text>().text = temp;
			}
			else if (counter < 0 && countLines + 1 == lineCount.Length)
			{
				counter = 5;
				textHolder.GetComponent<Text>().text = null;
				iconHolder.GetComponent<Image>().sprite = null;
				iconHolder.SetActive(false);
				background.SetActive(false);
				isCounting = false;
				countLines = 0;
			}
        }


    }

	void OnTriggerEnter(Collider other)
	{
        iconHolder.SetActive(true);
        background.SetActive(true);

		if (isWall == true)
		{
            temp = texts[Random.Range(0, texts.Length)];
			int temp_num = int.Parse(temp[0].ToString());
			iconHolder.GetComponent<Image>().sprite = sprites[temp_num];
		}
		else
		{
            isCounting = true;
            temp = texts[lineCount[0]];
			int temp_num = int.Parse(temp[0].ToString());
			iconHolder.GetComponent<Image>().sprite = sprites[temp_num];
			this.GetComponent<BoxCollider>().enabled = false;
		}

		temp = temp.Trim(temp[0]);
		textHolder.GetComponent<Text>().text = temp;
	}

	void OnTriggerExit(Collider other)
	{
        if (isWall == true)
        {
            textHolder.GetComponent<Text>().text = null;
            iconHolder.SetActive(false);
            background.SetActive(false);
        }
	}
}
