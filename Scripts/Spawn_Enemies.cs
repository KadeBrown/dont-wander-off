﻿using UnityEngine;
using System.Collections;


public class Spawn_Enemies : MonoBehaviour
{

	public GameObject trigger = null;

	public GameObject enemy1;
	public int amountOfEnemy1 = 0;
    public int waveIncreaseEnemy1 = 0;
	public GameObject enemy2;
	public int amountOfEnemy2 = 0;
    public int waveIncreaseEnemy2 = 0;
    public GameObject enemy3;
	public int amountOfEnemy3 = 0;
    public int waveIncreaseEnemy3 = 0;
    public GameObject target;
	public float spawnDelay = 0;
	public int amountOfWaves = 1;
    public int spawnRange = 10;

    //public int enemiesPerWave = 5;
    public bool wave = false;

	private bool waveRespawn;
	private System.Random rnd1 = new System.Random();
	private GameObject name = null;
	private int enemy1Count = 0;
	private int enemy2Count = 0;
	private int enemy3Count = 0;
	private bool noMoreSpawn = false;
	private float spawnCounter = 0;
	private int totalEnemies = 0;
	private int counter = 0;

	private int totalEnemy1Count = 0;
	private int totalEnemy2Count = 0;
	private int totalEnemy3Count = 0;
	private int waveCount = 0;

	// Use this for initialization
	void Start()
	{
		waveRespawn = false;
	}

	// Update is called once per frame
	void Update()
	{
		if (trigger == null || trigger.GetComponent<TriggerCall>().triggered == true)
		{
			spawnCounter += Time.deltaTime;

            float randPos1 = rnd1.Next(1, spawnRange);
            float randPos2 = rnd1.Next(1, spawnRange);
            float randPos3 = rnd1.Next(1, spawnRange);
            float randPos4 = rnd1.Next(1, spawnRange);

			totalEnemies = amountOfEnemy1 + amountOfEnemy2 + amountOfEnemy3;
			GameObject clone = null;

			SetName();

			//when no enemies are on the field
			if (wave == true && counter == 0 && waveCount < amountOfWaves)
			{
				totalEnemy1Count = 0;
				totalEnemy2Count = 0;
				totalEnemy3Count = 0;
				waveRespawn = true;
			}
			else if (wave == true && (totalEnemy1Count + totalEnemy2Count + totalEnemy3Count) == totalEnemies) // end of spawning enemies
			{
				waveCount += 1;
				waveRespawn = false;
				amountOfEnemy1 += waveIncreaseEnemy1; // Can change this to *times*
				amountOfEnemy2 += waveIncreaseEnemy2;
                amountOfEnemy3 += waveIncreaseEnemy3;
			}
			else if (wave == false && totalEnemies == totalEnemy1Count + totalEnemy2Count + totalEnemy3Count)
			{
				noMoreSpawn = true; // makes sure the enemies only spawn once, this can be done otherwise with just 1 wave
			}

			if (waveRespawn == true && spawnDelay < spawnCounter && waveCount <= amountOfWaves || wave == false && noMoreSpawn == false && spawnDelay < spawnCounter)
			{
				if (totalEnemy1Count + totalEnemy2Count + totalEnemy3Count < totalEnemies)
				{
					//spawn each type of enemy
					clone = Instantiate(name, new Vector3(this.transform.position.x - randPos1 + randPos2, this.transform.position.y,
											   this.transform.position.z - randPos3 + randPos4), this.transform.rotation) as GameObject;
					if (name == enemy1)
					{
						totalEnemy1Count += 1;
						enemy1Count += 1;
					}
					else if (name == enemy2)
					{
						totalEnemy2Count += 1;
						enemy2Count += 1;
					}
					else if (name == enemy3)
					{
						totalEnemy3Count += 1;
						enemy3Count += 1;
					}
					counter += 1;
					clone.transform.parent = this.transform;
					clone.GetComponent<BaseClass_Enemy>().target = target;
				}
				spawnCounter = 0;
			}
		}
	}


	void CounterChange(int id)
	{
		counter -= 1;
		if (id == 1)
		{
			enemy1Count -= 1;
		}
		else if (id == 2)
		{
			enemy2Count -= 1;
		}
		else if (id == 3)
		{
			enemy3Count -= 1;
		}
	}

	void SetName()
	{
		if (totalEnemy1Count < amountOfEnemy1 && totalEnemy2Count < amountOfEnemy2 && totalEnemy3Count < amountOfEnemy3)
		{
			int randNum = rnd1.Next(1, 4);

			if (randNum == 1)
			{
				name = enemy1;
			}
			else if (randNum == 2)
			{
				name = enemy2;
			}
			else if (randNum == 3)
			{
				name = enemy3;
			}
		}
		else if (totalEnemies != enemy1Count + enemy2Count + enemy3Count)
		{
			if (totalEnemy1Count < amountOfEnemy1)
			{
				name = enemy1;
			}
			else if (totalEnemy2Count < amountOfEnemy2)
			{
				name = enemy2;
			}
			else if (totalEnemy3Count < amountOfEnemy3)
			{
				name = enemy3;
			}
		}
	}


    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 1);
    }


}

