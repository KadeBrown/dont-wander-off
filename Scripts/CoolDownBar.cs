﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoolDownBar : MonoBehaviour {

	public enum Mode { Ice, Basic, Fire, Shield }
	public Mode type;
	public GameObject spellUser;

	private float cd = 0;
	private float cdwhole = 0;
	private float timer = 0;
    private bool isAwake;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (type == Mode.Ice)
		{
			cd = spellUser.GetComponent<Spell_IceCone>().GetCooldown();
			cdwhole = spellUser.GetComponent<Spell_IceCone>().coolDown;
		}
		if (type == Mode.Fire)
		{
			cd = spellUser.GetComponent<Spell_FireBall>().GetCooldown();
			cdwhole = spellUser.GetComponent<Spell_FireBall>().coolDown;
		}
		if (type == Mode.Basic)
		{
            isAwake = spellUser.GetComponent<Player_Shoot>().collected;
		}
        if (type == Mode.Shield)
        {
            cd = spellUser.GetComponent<Player_Shield>().GetCooldown();
            cdwhole = spellUser.GetComponent<Player_Shield>().coolDown;
        }
		
		//GetComponent<MeshRenderer>().material.SetFloat("_Completion",   cd / cdwhole );
		if(cd > cdwhole)
		{
			if(timer < 0.1)
			{
				timer += Time.deltaTime;
				this.GetComponent<Image>().color = Color.white;
			}
			else
			{ // makes it shine
				this.GetComponent<Image>().fillAmount = 0;
				this.GetComponent<Image>().color = Color.black;
			}
		}
		else
		{
		    timer = 0;
            if (type != Mode.Basic)
            {
                this.GetComponent<Image>().fillAmount = cd / cdwhole;
            }
            else
            {
                if (isAwake == true)
                {
                    this.GetComponent<Image>().fillAmount = 0;
                }
                else
                {
                    this.GetComponent<Image>().fillAmount = 100;
                }
            }

		}
        //Debug.Log(temp_cd);

    }
}
