﻿using UnityEngine;
using System.Collections;

public class Icon_Control : MonoBehaviour {

	//The icon gameobjects and the empty node gameobjects
	public GameObject mMIcon;
	public GameObject iSIcon;
	public GameObject fSIcon;
	public GameObject nodeTop;
	public GameObject nodeMiddle;
	public GameObject nodeBottom;

	//The player just to get the variables
	public GameObject player;

	// Update is called once per frame
	void Update () 
	{
	//Checks what spell the player has currently selected and moves the position of the spell and its scale according to the spell selection, the position on the 
	//icons are based on the empty gameobject nodes!
		if (player.GetComponent<SpellController> ().currentSpell == SpellController.Mode.Basic && mMIcon.transform.position != nodeMiddle.transform.position) 
		{
			mMIcon.transform.localScale = new Vector3(1.5f,1.5f,1.5f);
			mMIcon.transform.position = nodeMiddle.transform.position;
			iSIcon.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			iSIcon.transform.position = nodeTop.transform.position;
			fSIcon.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			fSIcon.transform.position = nodeBottom.transform.position;
		} 
		else if (player.GetComponent<SpellController> ().currentSpell == SpellController.Mode.Ice && iSIcon.transform.position != nodeMiddle.transform.position) 
		{
			mMIcon.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			mMIcon.transform.position = nodeBottom.transform.position;
			iSIcon.transform.localScale = new Vector3(1.5f,1.5f,1.5f);
			iSIcon.transform.position = nodeMiddle.transform.position;
			fSIcon.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			fSIcon.transform.position = nodeTop.transform.position;

		}
		else if (player.GetComponent<SpellController> ().currentSpell == SpellController.Mode.Fire && fSIcon.transform.position != nodeMiddle.transform.position) 
		{
			mMIcon.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			mMIcon.transform.position = nodeTop.transform.position;
			iSIcon.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
			iSIcon.transform.position = nodeBottom.transform.position;
			fSIcon.transform.localScale = new Vector3(1.5f,1.5f,1.5f);
			fSIcon.transform.position = nodeMiddle.transform.position;

		}
	}
}
