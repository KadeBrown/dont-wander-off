﻿using UnityEngine;
using System.Collections;

public abstract class BaseClass_Enemy : MonoBehaviour
{

	public int health = 100;
	public int type;
	public float speed = 13;
	public float acceleration = 60;
	public float howCloseToTarget = 1; // How close can the enemy get to the target, say Melee Range or Long Range? ya know
	public float detectionRadius = 5;
	public float kiteRange = 5;//should be less than howCloseToTarget
	public bool isFrozen = false;
	public bool canKite = false;
	public bool targetLocated = false;
	public bool hasIdleAnimation = false;
	public bool hasAttackAnimation = false;
	public bool hasDeathAnimation = false;
	public bool hasDrops = false;
	public GameObject gooObject;
	public GameObject target = null;
	public GameObject materialHolder;
	public GameObject targetBackup = null;
	public GameObject drops = null;
	public RaycastHit[] hits = null;
	[Range(0.0f, 100.0f)]
	public float dropChance;

	protected float delayTime = 2;
	private float colourCoolDown = 0;
	protected bool dying = false;
	private NavMeshAgent navAgent;
	private Vector3 lastKnownLocation = new Vector3(0, 0, 0);
	private Animator anim;

	// Use this for initialization
	protected void Start() 
	{
		targetBackup = target;
		navAgent = GetComponent<NavMeshAgent>();
		navAgent.speed = speed; // sets the speed
		navAgent.acceleration = acceleration; // this just makes sure there is no sliding effect to the enemies movement
		lastKnownLocation = this.transform.position;
		anim = GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	protected void Update()
	{
		//if enemy is not dying
		if (dying == false)
		{
			if ((targetBackup.transform.position - this.transform.position).magnitude > howCloseToTarget)
			{
				target = targetBackup;
			}

			//Makes the model flash red after being damaged
			if (colourCoolDown > 0)
			{
				colourCoolDown -= Time.deltaTime;
			}
			else if (colourCoolDown < 0)
			{
				colourCoolDown = 0;
				if (materialHolder.GetComponent<MeshRenderer>().Equals(null))
				{
					materialHolder.GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
				}
				else
				{
					materialHolder.GetComponent<MeshRenderer>().material.color = Color.white;
				}
			}
		}
		else
		{
			delayTime -= Time.deltaTime;

			if (delayTime < 0)
			{
				if (hasDrops == true && Random.Range(0, 100) <= dropChance)
				{
					GameObject clone = Instantiate(drops, this.transform.position + new Vector3(0, 2, 0), this.transform.rotation) as GameObject;
					clone.GetComponent<HealthPickup>().healthIndicator = GameObject.FindGameObjectWithTag("HealthContainer");
				}

				if (this.transform.parent == null)
				{
					Destroy(gameObject);
				}
				else
				{
					this.transform.parent.SendMessage("CounterChange", type);
					Destroy(gameObject);
				}
			}
		}
	}

	protected void FixedUpdate()
	{
		if (dying == true)
		{
			return;
		}
		if (target == null)
		{
			return;
		}
		hits = Physics.RaycastAll(this.transform.position, (target.transform.position - this.transform.position).normalized,
								  (target.transform.position - this.transform.position).magnitude);
		foreach (RaycastHit hit in hits)
		{
			if (hit.collider.gameObject.tag == "Wall")
			{
				targetLocated = false;
				break;
			}
			else
			{
				Collider[] cols = Physics.OverlapSphere(this.transform.position, detectionRadius);
				foreach (Collider c in cols)
				{
					if (c.gameObject.tag == "Enemy")
					{
						c.gameObject.GetComponent<BaseClass_Enemy>().targetLocated = true;
					}
				}
				targetLocated = true;
			}
		}

		if ((lastKnownLocation - this.transform.position).magnitude > 3 && targetLocated == true)
		{
			//the last know location is set to null

			lastKnownLocation = target.transform.position;
			//this.transform.LookAt(lastKnownLocation);
			HasAnimCheck(hasIdleAnimation, "isWalking", true);
		}
		else if ((lastKnownLocation - this.transform.position).magnitude < 3 && targetLocated == false)
		{
			navAgent.Stop();
			HasAnimCheck(hasIdleAnimation, "isWalking", false);
		}

		if (targetLocated == true)
		{
			this.transform.LookAt(targetBackup.transform.position);
			float mag = (target.transform.position - this.transform.position).magnitude;
			if (mag > howCloseToTarget)
			{
				navAgent.SetDestination(target.transform.position);
				navAgent.Resume();

			}
			else if (canKite == true && mag <= kiteRange)
			{
				Debug.Log("Kiting1");
				Vector3 fleePos = (this.transform.position - target.transform.position).normalized;
				navAgent.SetDestination(this.transform.position + fleePos * 2);
				navAgent.Resume();
			}
			else
			{
				navAgent.Stop();
			}
		}
	}

	/// <summary>
	/// Deals Damage and makes the model flash red
	/// </summary>
	/// <param name="hp"> The damage that will be dealt. </param>
	public void DealDamage(int damage)
	{
		health -= damage;

		if (health < 1)
		{
			GetComponent<Animator>().enabled = true;
			HasAnimCheck(hasDeathAnimation, "isDead", true);
			navAgent.Stop();
			gooObject.SetActive(true);
			dying = true;
			GetComponent<BoxCollider>().enabled = false;
		}

		colourCoolDown = 0.2f;
		if (materialHolder.GetComponent<MeshRenderer>().Equals(null))
		{
			materialHolder.GetComponent<SkinnedMeshRenderer>().material.color = Color.red;
		}
		else
		{
			materialHolder.GetComponent<MeshRenderer>().material.color = Color.red;
		}
	}

	/// <summary>
	/// Unfreezes the Enemy
	/// </summary>
	public void UnfreezeSelf()
	{
		isFrozen = false;
		if (materialHolder.GetComponent<MeshRenderer>().Equals(null))
		{
			materialHolder.GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
		}
		else
		{
			materialHolder.GetComponent<MeshRenderer>().material.color = Color.white;
		}
        this.GetComponent<Animator>().enabled = true;
		HasAnimCheck(hasIdleAnimation,"isWalking",true);
	}

	/// <summary>
	/// Check if the toCheck bool is true, if so attempt to change the bool in the animator (might need to change this in the future)
	/// </summary>
	/// <param name="toCheck">Which bool to check</param>
	/// <param name="boolToChange">what bool in the animator to change</param>
	/// <param name="valueToSet">its value to set </param>
	protected void HasAnimCheck(bool toCheck, string boolToChange, bool valueToSet)
	{
		//yield return new WaitForSeconds(delayTime);
		if (toCheck == true)
		{
			anim.SetBool(boolToChange, valueToSet);
		}
	}


	protected bool CheckAnimBool(bool toCheck, string boolToChange)
	{
		if (toCheck == true)
		{
			return anim.GetBool(boolToChange);
		}
		return false;
	}
}
