﻿using UnityEngine;
using System.Collections;

public class SpellController : MonoBehaviour
{

    public enum Mode { Ice = 1, Basic = 2, Fire = 3, None = 4 }


    public bool control_Ice = false;
    public bool control_Basic = false;
    public bool control_Fire = false;

    public int pickUpCountFire = 0;
    public int pickUpCountIce = 0;
    public int pickUpCountBasic = 0;


    public Mode currentSpell = Mode.None;
    public GameObject fireIdle = null;
    public GameObject iceIdle = null;
    public GameObject missileIdle = null;
    public GameObject arm = null;

    private float timer = 0;

    private bool inAction = false;
    private GameObject inUse = null;
    private GameObject activateAnim = null;
    private GameObject currentIdle = null;

    void Start()
    {
        GetComponent<Player_Shoot>().bullet.GetComponent<Bullet_Movement>().bulletSpeedAdd = 0;
        GetComponent<Spell_FireBall>().coolDownLess = 0;
        GetComponent<Spell_IceCone>().bullet.GetComponent<IceSpell_Movement>().coneObject.GetComponent<Spell_IceCone_Effect>().durationAdd = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        float mousehwheel = Input.GetAxis("Mouse ScrollWheel");
        if (mousehwheel < 0f && currentSpell != Mode.Fire && control_Fire == true || mousehwheel < 0f && currentSpell == Mode.Ice)
        {
            currentSpell += 1;
        }
        else if (mousehwheel > 0f && currentSpell != Mode.Ice && control_Ice == true || mousehwheel > 0f && currentSpell == Mode.Fire)
        {
            currentSpell -= 1;
        }
        else if (mousehwheel > 0f && currentSpell == Mode.Ice && control_Fire == true)
        {
            currentSpell += 2;
        }
        else if (mousehwheel < 0f && currentSpell == Mode.Fire && control_Ice == true)
        {
            currentSpell -= 2;
        }

        if (fireIdle != null && currentSpell == Mode.Fire && currentIdle != fireIdle)
        {
			currentIdle = fireIdle;
            if (inUse != null)
            {
                Destroy(inUse);
            }
            inUse = Instantiate(fireIdle, arm.transform.position - (arm.transform.forward * 0.4f), currentIdle.transform.rotation) as GameObject;
            inUse.transform.parent = arm.transform;
        }
        else if (iceIdle != null && currentSpell == Mode.Ice && currentIdle != iceIdle)
        {
			currentIdle = iceIdle;
            if (inUse != null)
            {
                Destroy(inUse);
            }
            inUse = Instantiate(iceIdle, this.transform.position + new Vector3(0, 7, 0), new Quaternion(90, 0, 0, 0)) as GameObject;

        }
        else if (missileIdle != null && currentSpell == Mode.Basic && currentIdle != missileIdle)
        {
			currentIdle = missileIdle;
            if (inUse != null)
            {
                Destroy(inUse);
            }
            inUse = Instantiate(missileIdle, arm.transform.position, currentIdle.transform.rotation) as GameObject;
            inUse.transform.parent = arm.transform;
        }

        if (inUse != null && currentIdle == fireIdle)
        {
            inUse.transform.position = arm.transform.position - (arm.transform.forward * 0.3f);
        }
		else if(inUse != null && currentIdle == missileIdle)
		{
			inUse.transform.position = arm.transform.position;
		}
        else if (inUse != null && currentIdle == iceIdle)
        {
            inUse.transform.position = this.transform.position + new Vector3(0, 7, 0);
        }

        if (this.GetComponent<Animator>().GetBool("isAttacking") == true)
        {
            timer += Time.deltaTime;
            if (timer > 2)
            {
                this.GetComponent<Animator>().SetBool("isAttacking", false);
                this.GetComponent<Animator>().SetLayerWeight(1, 0);
                timer = 0;
            }

        }

        if (inAction == false)
        {
            if (Input.GetKeyUp(KeyCode.Mouse0) && this.GetComponent<Spell_IceCone>().GetCooldown() >= this.GetComponent<Spell_IceCone>().coolDown && currentSpell == Mode.Ice)
            {
                this.GetComponent<Animator>().SetBool("isAttacking", true);
                this.GetComponent<Animator>().SetLayerWeight(1, 1);
                this.GetComponent<Spell_IceCone>().Invoke("UseIceCone", 0.5f);
                Invoke("ActionOver", 1f);
                inAction = true;
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0) && this.GetComponent<Player_Shoot>().collected == true && currentSpell == Mode.Basic)
            {
                this.GetComponent<Animator>().SetBool("isAttacking", true);
                this.GetComponent<Animator>().SetLayerWeight(1, 1);
                this.GetComponent<Player_Shoot>().Invoke("UseMagicMissle", 0.5f);
                Invoke("ActionOver", 1f);
                inAction = true;
            }
            else if (Input.GetKeyUp(KeyCode.Mouse0) && this.GetComponent<Spell_FireBall>().GetCooldown() >= this.GetComponent<Spell_FireBall>().coolDown && currentSpell == Mode.Fire)
            {
                this.GetComponent<Animator>().SetBool("isAttacking", true);
                this.GetComponent<Animator>().SetLayerWeight(1, 1);
                this.GetComponent<Spell_FireBall>().Invoke("UseFireBall", 0.5f);
                Invoke("ActionOver", 1f);
                inAction = true;
            }
        }


    }

    public void ActionOver()
    {
        inAction = false;
    }

    public void RemoveParticles()
    {
        if (activateAnim != null)
        {
            Destroy(activateAnim);
        }
        currentIdle = null;
        if (inUse != null)
        {
            Destroy(inUse);
        }
    }
}
