﻿using UnityEngine;
using System.Collections;

public class Spell_IceCone : MonoBehaviour {

	public float coolDown = 0;
	public GameObject bullet;
	public GameObject spawnPos;

	private float coolDownHolder = 0;
	// Use this for initialization
	void Start()
	{
		coolDownHolder = coolDown;
	}

	// Update is called once per frame
	void Update()
	{
		if (coolDownHolder <= coolDown)
		{
			coolDownHolder += Time.deltaTime;
		}
	}

	public float GetCooldown()
	{
		return coolDownHolder;
	}

	public void UseIceCone()
	{
		this.GetComponent<Animator>().SetBool("isAttacking", true);
		Instantiate(bullet, spawnPos.transform.position + this.transform.forward, this.transform.rotation);

		coolDownHolder = 0;
	}
}
