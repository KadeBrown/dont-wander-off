﻿using UnityEngine;
using System.Collections;

public class Spell_IceCone_Effect : MonoBehaviour
{
	
    public float duration = 3;
    public float durationAdd = 0;
    private float timer = 0;
    // Use this for initialization
    void Start()
    {
		//The duration addition that collecting collectables add onto.
        durationAdd = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer >= 0.1)
        {
			//Destroys the spell after its lived for a certain amount of time
            Destroy(gameObject.transform.GetChild(0).gameObject);
        }
        timer += Time.deltaTime;
    }

	/// <summary>
	/// Every Gameobject hit gets frozens for a given time and turns them blue for the duration of the freeze
	/// </summary>
	/// <param name="other">The collider that it hit</param>
    void OnTriggerEnter(Collider other)
    {
		
        other.gameObject.GetComponent<BaseClass_Enemy>().isFrozen = true;
		if (other.gameObject.GetComponent<BaseClass_Enemy>().materialHolder.GetComponent<MeshRenderer>().Equals(null))
		{
			other.gameObject.GetComponent<BaseClass_Enemy>().materialHolder.GetComponent<SkinnedMeshRenderer>().material.color = Color.blue;
		}
		else
		{
			other.gameObject.GetComponent<BaseClass_Enemy>().materialHolder.GetComponent<MeshRenderer>().material.color = Color.blue;
		}

		other.gameObject.GetComponent<NavMeshAgent>().Stop();
        other.gameObject.GetComponent<Animator>().enabled = false;
        other.gameObject.GetComponent<BaseClass_Enemy>().Invoke("UnfreezeSelf", duration + durationAdd);
    }
}
