﻿using UnityEngine;
using System.Collections;

public class TriggerForSpellActive : MonoBehaviour
{

    public enum Mode { Ice = 1, Basic = 2, Fire = 3 }
    public Mode currentSpell = Mode.Basic;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (currentSpell == Mode.Basic)
            {
                other.gameObject.GetComponent<SpellController>().pickUpCountBasic += 1;

                if (other.gameObject.GetComponent<SpellController>().pickUpCountBasic == 1 && other.gameObject.GetComponent<SpellController>().control_Basic == false)
                {
                    other.gameObject.GetComponent<SpellController>().control_Basic = true;
                    other.gameObject.GetComponent<SpellController>().pickUpCountBasic = 0;
                    other.gameObject.GetComponent<SpellController>().currentSpell = SpellController.Mode.Basic;
                }

                other.gameObject.GetComponent<Player_Shoot>().bullet.GetComponent<Bullet_Movement>().bulletSpeedAdd += 2f;
            }
            else if (currentSpell == Mode.Fire)
            {
                other.gameObject.GetComponent<SpellController>().pickUpCountFire += 1;

                if (other.gameObject.GetComponent<SpellController>().pickUpCountFire == 1 && other.gameObject.GetComponent<SpellController>().control_Fire == false)
                {
                    other.gameObject.GetComponent<SpellController>().control_Fire = true;
                    other.gameObject.GetComponent<SpellController>().pickUpCountFire = 0;
                    other.gameObject.GetComponent<SpellController>().currentSpell = SpellController.Mode.Fire;
                }
                other.gameObject.GetComponent<Spell_FireBall>().coolDownLess -= 0.2f;
            }
            else if (currentSpell == Mode.Ice)
            {
                other.gameObject.GetComponent<SpellController>().pickUpCountIce += 1;

                if (other.gameObject.GetComponent<SpellController>().pickUpCountIce == 1 && other.gameObject.GetComponent<SpellController>().control_Ice == false)
                {
                    other.gameObject.GetComponent<SpellController>().control_Ice = true;
                    other.gameObject.GetComponent<SpellController>().pickUpCountIce = 0;
                    other.gameObject.GetComponent<SpellController>().currentSpell = SpellController.Mode.Ice;
                }

                other.gameObject.GetComponent<Spell_IceCone>().bullet.GetComponent<IceSpell_Movement>().coneObject.GetComponent<Spell_IceCone_Effect>().durationAdd += 0.2f;
            }

            Destroy(gameObject);
        }
    }
}
