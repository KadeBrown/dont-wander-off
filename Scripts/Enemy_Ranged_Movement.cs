﻿using UnityEngine;
using System.Collections;

public class Enemy_Ranged_Movement : BaseClass_Enemy
{

    public float damage = 10;
    public float attackSpeed = 2;
    public GameObject bullet = null;
	public GameObject guuModel = null;
	public float delayTimer = 3.75f;
	public bool onCoolDown = false;
    private float timer = 0;
    // Use this for initialization
    void Start () 
	{
		base.Start();
		base.delayTime = 3.5f;
    }
	
	// Update is called once per frame
	void Update ()
	{
		base.Update();
		if (isFrozen == true)
		{
			return;
		}
		//Checks if the enemy is dead
		if (dying == true)
		{
			return;
		}
		//Checks if the attack animation is occuring
		if (onCoolDown == true)
		{
			delayTimer -= Time.deltaTime;
			//Applies the damage during the animation
			if (delayTimer <= 0)
			{
				guuModel.SetActive(false);
				delayTimer = 3.75f;
				targetBackup.gameObject.transform.parent.SendMessage("DealDamage", damage);
				onCoolDown = false;
				HasAnimCheck(hasAttackAnimation, "isAttacking", false);
				HasAnimCheck(hasAttackAnimation, "isWalking", false);
				GameObject clone = Instantiate(bullet, this.transform.position + this.transform.forward, this.transform.rotation) as GameObject;
				clone.GetComponent<Enemy_Projectile>().target = targetBackup;
			}
		}

        timer += Time.deltaTime;
        if ((this.transform.position - targetBackup.transform.position).magnitude <= howCloseToTarget)
        {
            this.GetComponent<NavMeshAgent>().Stop();
            if (attackSpeed < timer)
            {
				guuModel.SetActive(true);
				HasAnimCheck(hasAttackAnimation, "isAttacking", true);
				onCoolDown = true;
                timer = 0;
            }
               
        }
        else
        {
            this.GetComponent<NavMeshAgent>().Resume();
        }
    }

	void FixedUpdate()
	{
		if (isFrozen == true)
		{
			return;
		}
		base.FixedUpdate();
	}
}
