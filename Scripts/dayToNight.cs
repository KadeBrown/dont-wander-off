﻿using UnityEngine;
using System.Collections;

public class dayToNight : MonoBehaviour {

	public GameObject lightSource = null;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnTriggerEnter(Collider other)
	{
		lightSource.GetComponent<Light>().color = Color.black;
		Destroy(gameObject);
	}
}
