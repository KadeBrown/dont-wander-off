﻿using UnityEngine;
using System.Collections;

public class Enemy_Small : BaseClass_Enemy
{
    public float damage = 10;
    public float attackSpeed = 2;
    public float delayTimer = 1f;
    private bool onCoolDown = false;

    private float timer = 0;
	// Use this for initialization
	void Start ()
    {
		base.Start();
	}
	
	// Update is called once per frame
	void Update ()
    {
        base.Update();
        if (isFrozen == true)
        {
            return;
        }

        //Checks if the enemy is dead
        if (dying == true)
        {
            return;
        }
        //Checks if the attack animation is occuring
        if (onCoolDown == true)
        {
            delayTimer -= Time.deltaTime;
            //Applies the damage during the animation
            if (delayTimer <= 0)
            {
                delayTimer = 1f;
                targetBackup.gameObject.transform.parent.SendMessage("DealDamage", damage);
                onCoolDown = false;
                HasAnimCheck(hasAttackAnimation, "isAttacking", false);
                HasAnimCheck(hasAttackAnimation, "isWalking", false);
            }
        }
        //Cooldown Timer
        timer += Time.deltaTime;
        //If the Enemy is in distance of the player, stop moving and attack if the timer hit its mark.
        if ((this.transform.position - targetBackup.transform.position).magnitude <= howCloseToTarget)
        {
            this.GetComponent<NavMeshAgent>().Stop();
            if (attackSpeed < timer && targetBackup.GetComponentInParent<Player_Shield>().shieldActive == false)
            {
                HasAnimCheck(hasAttackAnimation, "isAttacking", true);
                timer = 0;
                onCoolDown = true;
            }
        }
        else
        {
            //if the Enemy isnt in range, go towards the player
            target = targetBackup;
            this.GetComponent<NavMeshAgent>().Resume();
        }
    }

	void FixedUpdate()
	{
		if (isFrozen == true)
		{
			return;
		}
		base.FixedUpdate();
	}
}
