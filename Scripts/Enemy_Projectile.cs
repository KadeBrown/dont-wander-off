﻿using UnityEngine;
using System.Collections;

public class Enemy_Projectile : MonoBehaviour {

    public GameObject target = null;

    public GameObject smallEnemy = null;

    private float heightChange = 0.1f;

	// Update is called once per frame
	void FixedUpdate ()
    {
        if (target == null)
        {
            return;
        }

       if (heightChange > 0 && heightChange < 5)
        {
            heightChange += Time.deltaTime * 50;
        }
        if (heightChange > 5)
        {
            heightChange = -10;
        }
		Vector3 temp_Pos = new Vector3((target.transform.position.x - this.transform.position.x) * 10, heightChange, (target.transform.position.z - this.transform.position.z) * 10);

		this.GetComponent<Rigidbody>().MovePosition(this.transform.position + temp_Pos * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Floor" || other.gameObject.tag == "Wall")
        {
            GameObject clone = Instantiate(smallEnemy, this.transform.position, this.transform.rotation) as GameObject;
            clone.GetComponent<BaseClass_Enemy>().target = target;
			clone.GetComponent<BaseClass_Enemy>().targetBackup = target;
          	//clone.transform.SetParent(this.transform.parent);
			clone.transform.LookAt(target.transform.position);
            Destroy(this.gameObject);
        }
		else if (other.gameObject.tag == "Player" && target.GetComponentInParent<Player_Shield>().shieldActive == false)
        {
            other.gameObject.SendMessage("DealDamage", smallEnemy.GetComponent<Enemy_Small>().damage);
            Destroy(this.gameObject);
        }
    }
}
