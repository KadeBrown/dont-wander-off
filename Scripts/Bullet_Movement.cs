﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Bullet_Movement : MonoBehaviour {
	
	//Sets all the basic variables that the bullet would need
    public float bulletSpeed = 100;
	public float bulletSpeedAdd = 0;
    public float bulletLife = 1;
	public float damage = 50;
    public int maxHits = 0;
	//This is the player
    public GameObject target = null; 
	// A list of all the hit enemies so they dont get hit more than once each phase of the bullet.
    public List<GameObject> alreadyHit = null;

	private Rigidbody rb;
    private bool toPlayer = false;

	// Use this for initialization
	void Start () 
    {
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        if (toPlayer == false)
        {
            rb.MovePosition(this.transform.position + this.transform.forward * (bulletSpeed + bulletSpeedAdd) * Time.deltaTime);
        }
        else
        {
            Vector3 temp_coOrds = (target.transform.position - this.transform.position).normalized;
            rb.MovePosition(this.transform.position + new Vector3(temp_coOrds.x,0,temp_coOrds.z) * (bulletSpeed + bulletSpeedAdd) * Time.deltaTime);
        }

        bulletLife -= Time.deltaTime;

        if (maxHits < 0 || bulletLife < 0)
        {
            TurnAround();
        }
	}

	/// <summary>
	/// Adds Enemies hit to the list of already hit enemies, if it collides with a Wall or Floor it returns back to the owner, if it hits the player it destroys itself
	/// </summary>
	/// <param name="other">The Collider hit</param>
	public void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy" && alreadyHit.Contains(other.gameObject) == false)
		{
			//Deals damage to the enemy and adds it to the hit list.
			other.gameObject.SendMessage("DealDamage", damage);
            maxHits -= 1;
            alreadyHit.Add(other.gameObject);
		}
        if (other.gameObject.tag == "Floor" || other.gameObject.tag == "Wall")
        {
            TurnAround();
        }
        if(other.gameObject.tag == "Player" && toPlayer == true)
        {
			//Makes sure the player can use the abiltiy again
            other.gameObject.GetComponent<Player_Shoot>().collected = true;
            Destroy(gameObject);
        }
	}

	/// <summary>
	/// Turns the bullet around to the player and resets the hit list to hit more enemies on the way back.
	/// </summary>
    public void TurnAround()
    {
        toPlayer = true;
        if (alreadyHit.Count > 0)
        {
            alreadyHit.Clear();
        }
    }
}
