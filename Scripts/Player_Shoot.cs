﻿using UnityEngine;
using System.Collections;

public class Player_Shoot : MonoBehaviour {

    public GameObject bullet;
	public GameObject spawnPos;
    public bool collected = true;

    public void UseMagicMissle()
    {
        this.GetComponent<Animator>().SetBool("isAttacking", true);
        GameObject clone = Instantiate(bullet, spawnPos.transform.position + this.transform.forward, this.transform.rotation) as GameObject;
        clone.GetComponent<Bullet_Movement>().target = gameObject;
        collected = false;
    }

}
