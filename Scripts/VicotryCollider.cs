﻿using UnityEngine;
using System.Collections;

public class VicotryCollider : MonoBehaviour {

	public GameObject victoryCanvas;


	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnTriggerEnter(Collider other)
	{
		GameObject[] temp_deathCleanUp = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] temp_deathCleanUp2 = GameObject.FindGameObjectsWithTag("EnemySpawner");
        GameObject[] temp_deathCleanUp3 = GameObject.FindGameObjectsWithTag("Wall");
        foreach (GameObject enemy in temp_deathCleanUp)
        {
            Destroy(enemy);
        }
        foreach (GameObject spawner in temp_deathCleanUp2)
        {
            Destroy(spawner);
        }
        foreach (GameObject wall in temp_deathCleanUp3)
        {
            Destroy(wall);
        }

        GameObject mainCam = GameObject.FindGameObjectWithTag("MainCamera");
        mainCam.transform.localPosition = new Vector3(0, 4.11f, -4.55f);

        mainCam.GetComponent<Camera>().orthographic = false;
		mainCam.transform.LookAt(other.gameObject.transform.position + new Vector3(0, 2, 0));
        // mainCam.transform.Rotate(11.5f, 0, 0, 0);

        //set position to the object you wish to spawn at
		other.gameObject.GetComponent<SpellController>().currentSpell = SpellController.Mode.None;
		other.gameObject.GetComponent<SpellController>().RemoveParticles();
		other.gameObject.GetComponent<SpellController>().enabled = false;
		other.gameObject.GetComponent<Look_Factor>().enabled = false;
		other.gameObject.transform.parent.GetComponent<Player_Shield>().enabled = false;
		other.gameObject.transform.parent.GetComponent<Player_Movement>().materialHolder.GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
		other.gameObject.transform.parent.GetComponent<Rigidbody>().useGravity = false;
		victoryCanvas.SetActive(true);

        GameObject mainUi = GameObject.FindGameObjectWithTag("UIMain");

        Destroy(mainUi);

		other.gameObject.transform.parent.GetComponent<Player_Movement>().enabled = false;
		Destroy(gameObject);
	}
}
