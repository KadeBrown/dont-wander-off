﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class exitPrevention : MonoBehaviour {

    public TextAsset textfile;
    public GameObject textDisplay = null;
    public string[] texts;
	// Use this for initialization
	void Start () 
    {
texts = textfile.ToString().Split('-');
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    void OnCollisionEnter(Collision other)
    {
        
        if(other.gameObject.tag == "Player")
        {
            textDisplay.GetComponent<Text>().text = texts[ Random.Range(0, texts.Length) ];
        }
    }
}
