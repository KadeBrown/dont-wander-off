﻿using UnityEngine;
using System.Collections;

public class FireBall_Movement : MonoBehaviour {

	public Vector3 target = Vector3.zero;
	public GameObject fireball;

	private float heightChange = 0.1f;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (target == Vector3.zero)
		{
			return;
		}
		
		if (heightChange > 0 && heightChange < 5)
		{
			heightChange += Time.deltaTime * 50;
		}
		if (heightChange > 5)
		{
			heightChange = -10;
		}

		Vector3 temp_Pos = new Vector3((target.x - this.transform.position.x )* 10, heightChange,( target.z - this.transform.position.z) * 10);

		this.GetComponent<Rigidbody>().MovePosition(this.transform.position + temp_Pos * Time.deltaTime);
	}

	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Floor" || other.gameObject.tag == "Wall")
		{
			Instantiate(fireball, this.transform.position, this.transform.rotation);
			Destroy(gameObject);
		}
	}
}
