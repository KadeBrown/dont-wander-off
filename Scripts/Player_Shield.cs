﻿using UnityEngine;
using System.Collections;

public class Player_Shield : MonoBehaviour
{

    //how long the shield should last
    public float shieldDuration = 0;
    public bool shieldActive = false;
    //how long the shield should be on cooldown
    public float coolDown = 5;
    public GameObject shield = null;
   
    //how long the shield has been active
    private float shieldTimer = 0;
    private float coolDownHolder = 0;
    private bool onCooldown = false;
    private GameObject activeShield = null;
   

    void Start()
    {
        coolDownHolder = coolDown;
    }

    // Update is called once per frame
    void Update()
    {
        if (coolDownHolder < coolDown && onCooldown == true)
        {
            coolDownHolder += Time.deltaTime;
        }

        if (onCooldown == true && coolDownHolder >= coolDown)
        {
            onCooldown = false;
            coolDownHolder = 0;
			shieldActive = false;
        }
        //if the shield has been active for more than 5 seconds
        if (shieldActive == true && shieldTimer > shieldDuration)
        {
            //get all collider within distance of 5
            Collider[] hits = Physics.OverlapSphere(transform.position, 10);

            //each enemy within the radius of 5
            foreach (Collider c in hits)
            {


                if (c.gameObject.tag == "Enemy")
                {
                    //push enemies away
                    Rigidbody body = c.gameObject.GetComponent<Rigidbody>();
                    Vector3 temp_Direction = (c.transform.position - transform.position).normalized;
                    //body.MovePosition(body.position + temp_Direction * 200 * Time.deltaTime);
                    body.AddForce(temp_Direction * 500, ForceMode.Impulse);
                }
            }
            //reset shield and timer
            shieldTimer = 0;
            onCooldown = true;
            if (activeShield != null)
            {
                Destroy(activeShield, 3);
            }
        }
        //press 'Right Mouse' to activate the shield
		if (onCooldown == false)
		{
			if (Input.GetKeyDown(KeyCode.Mouse1) && shieldActive == false && shield != null)
			{
				Debug.Log("rfrgesw");
				activeShield = Instantiate(shield, this.transform.position, shield.transform.rotation) as GameObject;
				activeShield.transform.SetParent(this.transform);
				activeShield.transform.localScale *= 1.5f;
				shieldActive = true;
			}
			//if the shield is active
			if (shieldActive == true)
			{
				//add to the timer
				shieldTimer += Time.deltaTime;
			}

			if (activeShield != null && activeShield.transform.localScale.magnitude > 0.4f)
			{
				activeShield.transform.localScale *= 0.996f;
			}
		}

    }
	/// <summary>
	/// Returns the coolDown
	/// </summary>
	/// <returns></returns>
    public float GetCooldown()
    {
        return coolDownHolder;
    }
}
