﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
	public GameObject player;
	public Sprite[] imageSprites = new Sprite[10];

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{

		float temp_Health = player.GetComponent<Player_Movement>().health;
		this.GetComponent<UnityEngine.UI.Image>().sprite = imageSprites[Mathf.CeilToInt(temp_Health / 10) - 1];
		//GetComponent<MeshRenderer>().material.SetFloat("_Completion", temp_Health / 100f);

	}
}
